import React, { PureComponent } from 'react'

import {
  Nav,
  Burger,
  List,
  ListItem,
  Link
} from './menu.styled.js'

export default class Menu extends PureComponent {
  state = {
    open: false
  }

  toggle = () => this.setState(state => ({ ...state, open: !state.open }))

  render() {
    const { open } = this.state

    return (
      <Nav open={open}>
        <Burger aria-hidden="true" aria-label="Menu" open={open} onClick={this.toggle} />
        <List open={open}>
          <ListItem><Link href="#" tabIndex="0">Home</Link></ListItem>
          <ListItem><Link href="#" tabIndex="0">Search</Link></ListItem>
          <ListItem><Link href="#" tabIndex="0">£8.55</Link></ListItem>
          <ListItem><Link href="#" tabIndex="0">Simon Rohrbach</Link></ListItem>
        </List>
      </Nav>
    )
  }
}