import styled, { css } from 'styled-components'
import { color, size, fontSize, media } from '../../styles/variables'

export const Nav = styled.nav`
  position: absolute;
  right: ${size.spacer};

  @media (${media.desktop}) {
    position: relative;
  }
`

const burgerStyle = css`
  width: calc(${size.spacer} * 2);
  border-bottom: 2px solid white;
  margin-bottom: calc(${size.spacer} / 2);

  &:focus,
  &:focus:after,
  &:focus:before {
    border-color: ${color.roo};
  }
`

const burgerOpen = css`
  transform: rotate(45deg);

  &:before {
    transform: rotate(90deg) translate(2px);
    margin: 0;
  }

  &:after {
    display: none;
  }
`

export const Burger = styled.button`
  ${burgerStyle}
  display: block;
  margin-bottom: 0px;

  background: transparent;
  cursor: pointer;
  outline: 0;
  transition: 100ms all ease-in-out;

  &:before,
  &:after {
    content: '';
    display: block;
    ${burgerStyle}
  }

  @media (${media.desktop}) {
    display: none;
  }

  ${props => props.open && burgerOpen}
`

const listOpen = css`
  display: flex;
  position: fixed;
  right: 0;
  left: 0;
`

export const List = styled.ul`
  padding: calc(${size.spacer} * 2);
  margin-top: ${size.spacer};
  display: none;
  flex-direction: column;

  background: ${color.black};
  text-align: center;

  @media (${media.desktop}) {
    width: 100%;
    margin: 0;
    padding: 0;
    display: flex;
    flex-direction: row;
    justify-content: space-evenly;
    position: relative;

    background: transparent;
  }

  ${props => props.open && listOpen};
`

export const ListItem = styled.li`
  line-height: ${fontSize.extraLarge};
  color: white;
`

export const Link = styled.a`
  outline: 0;

  &:hover,
  &:focus {
    color: ${color.roo};
  }

  @media (${media.desktop}) { 
    padding: calc(${size.spacer} * 1.5);
  }
`