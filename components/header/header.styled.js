import styled from 'styled-components'
import { color, font, size, fontSize, media, breakpoint } from '../../styles/variables'

import DeliverooLogo from '../../static/images/logo_mobile.svg'
import KangurooLogo from '../../static/images/logo.svg'

const local = {
  headerHeight: '45px',
  descriptionHeight: '500px'
}

export const Container = styled.header`
  height: ${size.header};

  display: flex;
  flex-direction: column;
  align-items: center;

  @media (${media.tablet}) {
    height: calc(${size.header} + calc(${size.spacer} * 2));
  }
`

export const Heading = styled.div`
  width: 100%;
  height: ${local.headerHeight};
  position: fixed;
  top: 0;

  background: ${color.black};
  z-index: 2;

  @media (${media.desktop}) {
    height: auto;
    position: absolute;
    top: ${size.spacer};
    left: 0;
    right: 0;

    background: transparent;
  }
`

export const HeadingContainer = styled.div`
  max-width: ${breakpoint.desktop};
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 0 auto;
  position: relative;

  @media (${media.desktop}) {
    padding: ${size.spacer};
    justify-content: space-between;
  }
`

export const TextLogo = styled(DeliverooLogo)`
  display: block;
  
  text-indent: -999px;
  overflow: hidden;

  @media (${media.desktop}) {
    display: none;
  }
`

export const Logo = styled(KangurooLogo)`
  display: none;

  @media (${media.desktop}) {
    display: block;
  }
`

export const Neighbourhood = styled.section`
  padding: calc(${size.spacer} * 4);
  margin-top: ${local.headerHeight};
  position: absolute;

  text-align: center;
  color: white;
  z-index: 1;

  @media (${media.desktop}) {
    margin-top: calc(calc(${size.spacer} * 4) * 2);
  }
`

export const Restaurants = styled.strong`
  text-transform: uppercase;
  font-weight: 300;
  font-size: ${fontSize.small};
  color: ${color.greyDark};
  letter-spacing: 2px;
`

export const Title = styled.h1`
  text-transform: uppercase;
  font-size: ${fontSize.extraLarge};
  font-weight: 400;
  letter-spacing: calc(${size.spacer} / 2);
`

export const Description = styled.p`
  max-width: ${local.descriptionHeight};

  font-size: ${fontSize.regular};
  font-weight: 100;
  line-height: ${fontSize.large};

  @media (${media.desktop}) {
    line-height: calc(${fontSize.extraLarge} - 4px);
  }
`