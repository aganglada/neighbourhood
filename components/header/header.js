import React, { PureComponent } from 'react'
import dynamic from 'next/dynamic'

import Menu from '../menu/menu'
const Image = dynamic(import('../lazyImage/lazyImage'))

import {
  Container,
  Heading,
  HeadingContainer,
  TextLogo,
  Logo,
  Neighbourhood,
  Restaurants,
  Title,
  Description
} from './header.styled'

export default class Header extends PureComponent {
  shouldComponentUpdate() {
    return false
  }

  render() {
    const { images, name, description, total } = this.props

    return (
      <Container>
        <Image images={images} />
        <Heading>
          <HeadingContainer>
          <TextLogo>Deliveroo</TextLogo>
          <Logo />
          <Menu />
          </HeadingContainer>
        </Heading>
        <Neighbourhood>
          <Restaurants>{total} restaurants delivering to</Restaurants>
          <Title>{name}</Title>
          <Description>{description}</Description>
        </Neighbourhood>
      </Container>
    )
  }
}