import styled, { css } from 'styled-components'
import { color, fontSize, size, media } from '../../styles/variables'
import ArrowRight from '../../static/images/arrow_right.svg'

export const Link = styled.a`
  display: flex;
  justify-content: space-around;
  align-items: center;
  padding: ${size.spacer} 0;

  @media (${media.desktop}) {
    padding: 0;
  }
`

export const Name = styled.h3`
  width: 50%;
  font-size: ${fontSize.large};
  font-weight: 500;
`

export const Info = styled.div`
  display: flex;
  flex-direction: column-reverse;

  font-size: ${fontSize.regular};
  color: ${color.grey};
  text-align: right;

  @media (${media.tablet}) {
    width: 40%;
    flex-direction: row;
    justify-content: space-between;
  }
`

export const DeliveryTime = styled.span``

export const Category = styled.span`
  display: none;

  @media (${media.tablet}) {
    display: inline-block;
  }
`

export const Type = styled.span``

export const Next = styled(ArrowRight)`
  @media (${media.desktop}) {
    display: none;
  }
`