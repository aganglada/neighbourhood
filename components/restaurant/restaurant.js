import React, { PureComponent } from 'react'

import {
  Link,
  Name,
  DeliveryTime,
  Category,
  Type,
  Info,
  Next
} from './restaurant.styled.js'

export default class Restaurant extends PureComponent {
  render() {
    const { info, currencySymbol } = this.props

    return (
      <Link href="#">
        <Name>{info.name}</Name>
        <Info>
          <Type>{info.type}</Type>
          <Category>{currencySymbol.repeat(info.category)}</Category>
          <DeliveryTime>{info.deliveryTime}</DeliveryTime>
        </Info>
        <Next />
      </Link>
    )
  }
}