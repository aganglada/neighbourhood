import React, { PureComponent } from 'react'

import { media } from '../../styles/variables'
import { matchImageWithScreen } from '../../services/screen/screen'

import {
  CoverImage
} from './lazyImage.styled'

const imageSizeMatches = {
  small: media.mobile,
  medium: media.tablet,
  large: media.desktop
}

export default class LazyImage extends PureComponent {

  state = {
    loaded: false,
    error: false
  }

  componentDidMount() {
    const size = matchImageWithScreen(imageSizeMatches)
    const img = new Image()
    img.src = this.props.images[size]
    img.onload = ({ target }) => {
      if ('naturalHeight' in target) {
        return target.naturalHeight + target.naturalWidth === 0
          ? this.onLoadError()
          : this.onLoadSuccess()
      }

      return target.height + target.width === 0
        ? this.onLoadError()
        : this.onLoadSuccess()
    }
  }

  onLoadError = () => {
    this.setState(state => ({ ...state, loaded: true, error: 'Unable to load image' }))
    if (typeof this.props.onError === 'function') {
      this.props.onError()
    }
  }

  onLoadSuccess = () => {
    this.setState(state => ({ ...state, loaded: true }))
    if (typeof this.props.onSuccess === 'function') {
      this.props.onSuccess()
    }
  }

  render() {
    const { loaded, error } = this.state
    const { images } = this.props

    return <CoverImage loaded={loaded} images={images} />
  }
}