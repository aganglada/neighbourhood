import styled, { css } from 'styled-components'
import { color, size, media } from '../../styles/variables'

const image = css`
  margin: 0;
  height: 100%;
  width: 100%;
  padding-bottom: ${size.header};

  background-color: ${color.greyLight};
  background-repeat: no-repeat;
  background-size: cover;
  filter: brightness(40%);
`

const blurryImage = css`
  ${image}

  background-image: url('${props => props.images.tiny}');
  filter: blur(3px) brightness(80%);
`

const coverImage = css`
  ${image}

  background-image: url('${props => props.images.small}');
  transition: filter 500ms ease;

  @media (${media.mobile}) {
    background-image: url('${props => props.images.medium}');
    filter: brightness(60%);
  }

  @media (${media.desktop}) {
    background-image: url('${props => props.images.large}');
    filter: brightness(100%);
  }
`

export const CoverImage = styled.figure`
  ${props => props.loaded ? coverImage : blurryImage}
`