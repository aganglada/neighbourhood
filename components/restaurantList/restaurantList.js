import React, { PureComponent } from 'react'

import {
  List,
  ListItem
} from './restaurantList.styled.js'

import Restaurant from '../restaurant/restaurant'

export default class RestaurantList extends PureComponent {
  render() {
    const { restaurants, currencySymbol } = this.props

    return (
      <List>{
        restaurants.map((restaurant, i) => {
          return (
            <ListItem key={i}>
              <Restaurant
                info={restaurant}
                currencySymbol={currencySymbol}
              />
            </ListItem>
          )
        })
      }</List>
    )
  }
}