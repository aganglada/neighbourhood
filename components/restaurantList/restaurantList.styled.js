import styled, { css } from 'styled-components'
import { breakpoint, size, media, color } from '../../styles/variables'

export const List = styled.ul`
  max-width: ${breakpoint.desktop};
  margin: 0;

  @media (${media.desktop}) {
    margin: calc(${size.spacer} * 4) auto;
  }
`

export const ListItem = styled.li`
  border-bottom: 1px solid ${color.greyLight};

  @media (${media.desktop}) {
    &:first-child {
      border-top: 1px solid ${color.greyLight};
    }
  }
`