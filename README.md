# Tech stack

* nextjs
* react
* styled-components

* Pa11y and Lighthouse to check accesibility, performance and PWA.

# Running it locally

```
git clone git clone git@bitbucket.org:aganglada/neighbourhood.git

cd neighbourhood

npm i / yarn

yarn dev / npm run dev
```

# Things to look at (design/a11y)

* Grey on restaurant info have no sufficient contrast ratio
* Width of description in desktop is 507 instead 500
* Looking for a good range of sizes

