import Head from 'next/head'
import React, { PureComponent } from 'react'

import '../styles/base'

import { 
  fetchNeighbourhood, 
  getNeighbourhood,
  getCurrencySymbol
} from './index.service'
import {
  Container
} from './index.styled.js'

import Header from '../components/header/header'
import RestaurantList from '../components/restaurantList/restaurantList'

export default class Neighbourhood extends PureComponent {
  static async getInitialProps() {
    const name = getNeighbourhood()
    const currencySymbol = getCurrencySymbol()
    const neighbourhood = await fetchNeighbourhood(name)

    return {
      neighbourhood,
      currencySymbol
    }
  }

  render() {
    const { neighbourhood, currencySymbol } = this.props

    return (
      <Container>
        <Head>
          <title>Takeaway delivery in {neighbourhood.name} - Order with Deliveroo</title>
          <meta name="theme-color" content="#000" />
          <meta name="description" content={neighbourhood.description} />
          <link href="/static/icons/favicon.ico" rel="shortcut icon" />
        </Head>
        <Header
          name={neighbourhood.name}
          description={neighbourhood.description}
          images={neighbourhood.images}
          total={neighbourhood.total}
        />
        <RestaurantList
          restaurants={neighbourhood.restaurants}
          currencySymbol={currencySymbol}
        />
      </Container>
    )
  }
}