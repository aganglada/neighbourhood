import Document, { Head, Main, NextScript } from 'next/document'
import { ServerStyleSheet } from 'styled-components'

export default class MyDocument extends Document {
  render () {
    const sheet = new ServerStyleSheet()
    const main = sheet.collectStyles(<Main />)
    const styles = sheet.getStyleElement()

    return (
     <html lang="en">
       <Head>
        <link rel="manifest" href="/static/manifest.json" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <style>{styles}</style>
       </Head>
       <body>
         {main}
         <NextScript />
       </body>
     </html>
    )
  }
}