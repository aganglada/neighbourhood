import { get } from '../services/api/api'

import config from '../config/neighbourhood.json'

export const getNeighbourhood = () => {
  return config.name
}

export const getCurrencySymbol = () => {
  return config.currencySymbol
}

export const fetchNeighbourhood = (neighbourhood) => {
  return get(`/static/${neighbourhood}.json`)
}