import fetch from 'isomorphic-fetch'
import config from '../../config/api.json'

const request = (url, options) => {
  return fetch(`${config.BASE_URL}${url}`, options)
    .then(response => response.json())
}

export const get = (url, options) => {
  return request(url, options)
}