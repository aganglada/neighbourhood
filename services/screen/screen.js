export const matchImageWithScreen = (screens) => {
  const sizes = Object.keys(screens)
  let size = sizes[0]

  sizes.forEach(screen => {
    if (matchMedia(screens[screen])) {
      size = screen
    }
  })

  return size
}

const matchMedia = (media) => {
  return window.matchMedia(`(${media})`).matches
}