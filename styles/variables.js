export const color = {
  black: '#000',
  grey: '#666',
  greyDark: 'rgba(255, 255, 255, .6)',
  greyLight: '#E5E5E5',
  roo: '#00ccbc'
}

export const font = {
  regular: 'Proxima Nova'
}

export const fontSize = {
  small: '12px',
  regular: '14px',
  large: '18px',
  extraLarge: '28px'
}

export const media = {
  mobile: 'min-width: 450px',
  tablet: 'min-width: 768px',
  desktop: 'min-width: 960px',
}

export const breakpoint = {
  desktop: '1280px'
}

export const size = {
  spacer: '10px',
  header: '320px'
}